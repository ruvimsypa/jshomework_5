let user = {
  name: 'Таня',
  age: 25,
  size: {
    top: 90,
    middle: 60,
    bottom: 90
  }
}
let userClone = {};

function clone(value1, value2){
    for(let key in value1){
        value2[key] = value1[key];
    }
    console.log(value1,value2);
}

clone(user, userClone);
userClone.name = 'Алёна';
console.log(user,userClone);